package soma.model;

import java.util.Arrays;

import soma.model.Rotations.Matrix;

/**
 * A PieceInstance represents an instance of a specific soma cube {@link Piece}.
 * An instance is defined by the piece type, and the collection of {@link Point}
 * objects, which define the locations of the cuboids of the piece. A piece is w
 * 
 *
 */
public class PieceInstance {
	/*
	 * The type of this piece
	 */
	private Piece p;

	/*
	 * The actual coordinates of the cubes belonging to this piece.
	 */
	private Point[] points;

	public PieceInstance(Piece p, Point[] points){
		super();
		this.p = p;
		this.points = points;
		Arrays.sort(points);
	}

	@Override
	public String toString(){
		return Arrays.toString(points);
	}

	public void fit(){
		while (Arrays.stream(points).anyMatch(p -> p.getX() < 0)){
			Arrays.stream(points).forEach(p -> p.transX(1));
		}
		while (Arrays.stream(points).anyMatch(p -> p.getY() < 0)){
			Arrays.stream(points).forEach(p -> p.transY(1));
		}
		while (Arrays.stream(points).anyMatch(p -> p.getZ() < 0)){
			Arrays.stream(points).forEach(p -> p.transZ(1));
		}
	}

	void transX(int amount){
		Arrays.stream(points).forEach(p -> p.transX(amount));

		Arrays.sort(points);
	}

	void transY(int amount){
		Arrays.stream(points).forEach(p -> p.transY(amount));

		Arrays.sort(points);
	}

	void transZ(int amount){
		Arrays.stream(points).forEach(p -> p.transZ(amount));

		Arrays.sort(points);
	}

	public Point[] getPoints(){
		return points;
	}

	public PieceInstance copy(Matrix rotation){
		Point[] pts = new Point[points.length];
		for (int i = 0; i < points.length; i++){
			pts[i] = new Point(points[i].getX(), points[i].getY(), points[i].getZ());
		}

		pts = Rotations.rotatePoints(pts, rotation);
		return new PieceInstance(this.getPiece(), pts);

	}

	public PieceInstance copy(){
		Point[] pts = new Point[points.length];
		for (int i = 0; i < points.length; i++){
			pts[i] = new Point(points[i].getX(), points[i].getY(), points[i].getZ());
		}
		return new PieceInstance(this.getPiece(), pts);

	}

	boolean inside(){
		return Arrays.stream(points).allMatch(pt -> pt.getX() < 3 && pt.getX() >= 0) && //
				Arrays.stream(points).allMatch(pt -> pt.getY() < 3 && pt.getY() >= 0) && //
				Arrays.stream(points).allMatch(pt -> pt.getZ() < 3 && pt.getZ() >= 0);
	}

	public boolean overLaps(PieceInstance in){
		for (Point p : points){
			for (Point o : in.points){
				if (p.equals(o)){
					return true;
				}
			}
		}
		return false;
	}

	public Piece getPiece(){
		return p;
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getPiece() == null) ? 0 : getPiece().hashCode());
		result = prime * result + Arrays.hashCode(points);
		return result;
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PieceInstance other = (PieceInstance) obj;
		if (getPiece() != other.getPiece())
			return false;
		if (!Arrays.equals(points, other.points))
			return false;
		return true;
	}
}
