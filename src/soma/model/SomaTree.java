package soma.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

public class SomaTree {
	/*
	 * When a node does not get any children during a specific piece, more pieces
	 * will not fit anymore. This queue is used to store the reference to those that
	 * are waiting deletion.
	 * 
	 * We cannot directly delete the node, because it is possible that it is the
	 * final child, thus the parent might need to deleted as well. Deleting directly
	 * is not possible, as during adding of pieces the list of parents and their
	 * parents are being iterated.
	 */
	private Queue<SomaNode> toDelete = new ArrayDeque<>();

	private SomaNode root;

	private static int nextSolutionID = 0;

	/**
	 * Initialize a tree with certain PieceInstance as the root node.
	 * 
	 * @param rootPiece the first piece in the attempt to generate the cube.
	 */
	public SomaTree(PieceInstance rootPiece){
		root = new SomaNode(rootPiece);

	}

	/**
	 * Add a list of PieceInstances.
	 * 
	 * @param a list of PieceInstances to be added next.
	 */
	public void addPiece(List<PieceInstance> pieces){
		root.addChildren(pieces);
		deleteChildles();
	}

	/**
	 * Add a list of PieceInstances.
	 * 
	 * @param a list of PieceInstances to be added next.
	 */
	public void addPiece(Piece p){
		addPiece(p.getAllLocations());
		deleteChildles();
	}

	/**
	 * Iterate the delete nodes
	 */

	private void deleteChildles(){
		while (!toDelete.isEmpty()){
			SomaNode node = toDelete.poll();
			ArrayList<SomaNode> nodes = node.parent.children;
			nodes.remove(node);
			if (nodes.isEmpty()){
				toDelete.add(node.parent);
			}
		}

	}

	public ArrayList<SomaSolution> traverse(){
		ArrayList<SomaSolution> solutions = new ArrayList<>();
		ArrayList<String> id = new ArrayList<>();
		root.getSolutions(solutions, id);
		return solutions;
	}

	private class SomaNode {

		private SomaNode parent;
		private PieceInstance piece;
		private char[][][] cubeState;
		private ArrayList<SomaNode> children;

		private int depth = 0;

		public SomaNode(PieceInstance piece){
			this.piece = piece;
			cubeState = new char[3][3][3];
			for (int i = 0; i < 3; i++){
				for (int j = 0; j < 3; j++){
					for (int k = 0; k < 3; k++){
						cubeState[i][j][k] = '0';
					}
				}
			}
			// Because this does not have a parent, we are dealing with root node.
			// We can place the piece without checking it: it will fit anyway.
			validateLocation();
		}

		/**
		 * Initialize a node with specific parent node
		 * 
		 * @param piece  certain PieceInstance
		 * @param parent parent node
		 */
		public SomaNode(PieceInstance piece, SomaNode parent){
			this.piece = piece;
			this.cubeState = parent.copyCube();
			this.parent = parent;
			this.depth = parent.depth + 1;
		}

		/**
		 * Puts the piece in the given location. Returns false, if this is not possible.
		 * This method is safe even if called multiple times. Valid places include empty
		 * space and the piece itself.
		 * 
		 * @return true if the piece fits and false if not.
		 */
		public boolean validateLocation(){
			for (Point pp : piece.getPoints()){
				int x = pp.getX();
				int y = pp.getY();
				int z = pp.getZ();

				if (cubeState[x][y][z] == '0'){
					cubeState[x][y][z] = piece.getPiece().LETTER;
				}else{
					// there is something else, this piece did not fit.
					return false;
				}
			}
			return true;
		}

		/**
		 * If this node is a leaf node without any children initialized, this method
		 * adds the given pieces as children to this node. Only such pieces that
		 * actually fit are added.
		 * 
		 * If this node already has children, the task is passed to each of them.
		 * 
		 * @param pieces to be added into the cube
		 */
		public void addChildren(List<PieceInstance> pieces){
			// if this does not have children yet, we reached a leaf node.
			if (children == null){
				children = new ArrayList<>();
				for (PieceInstance p : pieces){
					SomaNode child = new SomaNode(p, this);
					if (child.validateLocation()){
						children.add(child);
					}
				}
				// after adding children, if none were added, we reached a terminating node.
				// parent can remove this node.
				if (children.size() == 0){
					toDelete.add(this);
				}
			}else{
				// if this node already had children, add all the pieces to each rec
				for (SomaNode childNode : children){
					childNode.addChildren(pieces);
				}
			}
		}

		/**
		 * Copy the contents of this cube
		 * 
		 * @return new char[][][] with same contents as this SomaNode
		 */
		private char[][][] copyCube(){
			char[][][] chars = new char[3][3][3];
			for (int i = 0; i < 3; i++){
				for (int j = 0; j < 3; j++){
					for (int k = 0; k < 3; k++){
						chars[i][j][k] = cubeState[i][j][k];
					}
				}
			}
			return chars;
		}

		/**
		 * If this is a terminating node (no children) we are at the bottom of the tree.
		 * This adds the piece to the list of solutions
		 * 
		 * @param accumulator
		 */
		public void getSolutions(ArrayList<SomaSolution> accumulator, ArrayList<String> id){
			if (this.children == null){
				accumulator.add(new SomaSolution(cubeState));
			}else{
				for (SomaNode s : children){
					s.getSolutions(accumulator, id);
				}
			}
		}

	}

	static int nextID(){
		return nextSolutionID++;

	}

	public class SomaSolution {
		private char[][][] cube;
		private int id;
		private int mirrorID = -1;

		private SomaSolution(char[][][] cube){
			this.cube = cube;
			this.id = nextID();
		}

		@Override
		public String toString(){
			StringBuilder sb = new StringBuilder();
			for (int z = 0; z < 3; z++){
				for (int y = 0; y < 3; y++){

					for (int x = 0; x < 3; x++){
						sb.append(cube[x][y][z]);
					}
					sb.append(" ");

				}
				sb.append("\n");
			}
			return sb.toString();
		}

		public char middleChar(){
			return cube[1][1][1];
		}

		public void findMirror(ArrayList<SomaSolution> solutions){
			if (mirrorID > 0){
				return;
			}
			for (SomaSolution s : solutions){
				if (isMirror(cube, s.cube)){
					s.mirrorID = this.id;
					this.mirrorID = s.id;
					System.out.println(id + " " + mirrorID);
					return;
				}
			}
		}

		private boolean isMirror(char[][][] self, char[][][] other){
			for (int z = 0; z < 3; z++){
				for (int y = 0; y < 3; y++){
					for (int x = 0; x < 3; x++){
						if (self[x][y][z] == 'A' || self[x][y][z] == 'B')
							continue;
						if (self[x][y][z] != other[3 - 1 - x][y][z]){
							return false;
						}
					}
				}
			}
			return true;
		}

		public String serializedData(){
			String val = "# id number by order of extraction from the tree\n" + "id=%d\n"
					+ "#id of the mirrored solution\nmirror=%d\n" + "data=%s\n";

			return String.format(val, id, mirrorID, Arrays.deepToString(cube));
		}

		public String serializedData2(){
			StringBuilder val = new StringBuilder("# id number by order of extraction from the tree\n" + "id=%d\n"
					+ "#id of the mirrored solution\nmirror=%d\n");
			for (int z = 0; z < 3; z++){
				for (int y = 0; y < 3; y++){
					for (int x = 0; x < 3; x++){
						val.append(String.format("%s,%s,%s,%s\n", x, y, z, cube[x][y][z]));
					}
				}
			}

			return String.format(val.toString(), id, mirrorID);
		}

		public String asPyArrays(){
			HashMap<Character, StringBuilder> strs = new HashMap<>();

			for (int z = 0; z < 3; z++){
				for (int y = 0; y < 3; y++){
					for (int x = 0; x < 3; x++){
						char c = cube[x][y][z];
						StringBuilder code = strs.getOrDefault(c, new StringBuilder());
						String next = String.format(" ((x==%d) & (z==%d) & (y==%d)) ", x, y, z);
						if (code.length() == 0){
							code.append("(\"" + c + "\"," + next + ")");
						}else{
							code.insert(code.length() - 1, "|" + next);
						}
						strs.put(c, code);
					}
				}
			}
			String combined = strs.values().stream().map(x -> x.toString()).reduce((x, y) -> x + "," + y).get();
			return String.format("solutions.append(([%s],%d,%d))", combined, id, mirrorID);
		}

		public int getID(){
			return id;
		}
	}
}
