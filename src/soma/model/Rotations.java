package soma.model;

import java.util.ArrayList;

/*
 * Rotation matrices for each counter clockwise 90 degree turn.
 */
public class Rotations {

	public enum Matrix {
		ROT90_X(rot90x),
		ROT90_Y(rot90y),
		ROT90_Z(rot90z);

		private final int[][] matrix;

		private Matrix(int[][] matrix){
			this.matrix = matrix;
		}

	}

	private static final int[][] rot90x = new int[][]{ //
			{ 1, 0, 0 }, //
			{ 0, 0, -1 }, //
			{ 0, 1, 0 } //
	};

	private static final int[][] rot90y = new int[][]{ //
			{ 0, 0, 1 }, //
			{ 0, 1, 0 }, //
			{ -1, 0, 0 } //
	};

	private static final int[][] rot90z = new int[][]{ //
			{ 0, -1, 0 }, //
			{ 1, 0, 0 }, //
			{ 0, 0, 1 } //
	};

	public static int multiply(int x, int y, int z, int[] vals){
		return(x * vals[0] + y * vals[1] + z * vals[2]);
	}

	public static Point[] rotatePoints(Point[] p, Matrix rotm){
		ArrayList<Point> newpoints = new ArrayList<>();
		for (Point point : p){
			int px = point.getX();
			int py = point.getY();
			int pz = point.getZ();

			newpoints.add(new Point(multiply(px, py, pz, rotm.matrix[0]), multiply(px, py, pz, rotm.matrix[1]),
					multiply(px, py, pz, rotm.matrix[2])));
		}
		return newpoints.toArray(new Point[0]);
	}
}
