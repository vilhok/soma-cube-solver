package soma.model;

import java.util.Comparator;

/**
 * A mutable point in 3 dimensions
 * 
 *
 */
public final class Point implements Comparable<Point> {
	private int x;
	private int y;
	private int z;

	private static Comparator<Point> comparator = Comparator.comparing(Point::getX)//
			.thenComparing(Point::getY)//
			.thenComparing(Point::getZ);

	public Point(int x, int y, int z){
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public String toString(){
		return "{" + x + "," + y + "," + z + "}";
	}

	public void transX(int amount){
		x += amount;
	}

	public void transY(int amount){
		y += amount;
	}

	public void transZ(int amount){
		z += amount;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	public int getZ(){
		return z;
	}

	@Override
	public int compareTo(Point o){
		return comparator.compare(this, o);
	}

	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + z;
		return result;
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		if (z != other.z)
			return false;
		return true;
	}

	public Point copy(){
		return new Point(x, y, z);
	}

}